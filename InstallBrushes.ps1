<#
.Synopsis
   A PowerShell script to install Photoshop brushes. Note: Photoshop will launch when the script starts.
.DESCRIPTION
   A PowerShell script to install Photoshop brushes in a selected (or current) directory and all subdirectories. It will currently install ABR format brushes. Note: Photoshop will launch when the script starts.
.EXAMPLE
   PS D:\Brushes> Add-Brushes

.EXAMPLE
   PS D:\> Add-Brushes -InstallPath D:\Brushes
#>
function Add-Brushes
{
    [CmdletBinding()]
    Param
    (
        # The location of the folder where your Photoshop brushes are stored.
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $InstallPath = "."
    )

    Begin
    {
        #Change to the location where the brushes are installed
        Set-Location -Path $InstallPath

        #Get all brushes in all directories and subdirectories.
        $Brushes = Get-ChildItem -Recurse -Force -Include *.abr
    }
    Process
    {
        foreach ($brush in $Brushes)
        {
            #Brushes are executable (sort of) so we install them just by invoking them.
            Write-Verbose "Installing $($brush)"
            Invoke-Item $brush
        }
    }
}