# README #

A PowerShell script that will install all Photoshop brushes in a directory and all it's sub-directories.

The script was created for my wife. We recently bought her a new Windows 8.1 machine for her to do all of her design/technical writing work, and she had collected a large number of brushes over time. All of these fonts are in ABR format, and she has been putting them in a single folder on her external drive. Well since we will soon be getting her a better SSD, the laptop might end up reloaded again. Beside that, I'm sure she will go on another spree where she downloads a ton of brushes and needs to install them, so this script will work for that as well.

Similar to this, I wrote a script to [install fonts](https://bitbucket.org/bradwhitfield/install-fonts) as well.

##Note!##

This comes with no warranty! Read the script before you blindly trust it. I really can't think of how it would mangle your machine, unless you had a bad brushes, but you still need to be responsible with any script you find on the web.

Photoshop will open when this script starts. That is just due to file association with the ABR files.

### Script Info ###

* Tested on Windows 10, 8.1, and 8
* Tested with PowerShell version 3, 4, and 5
* Tested with Photoshop CC

### How do I run this? ###

1. Download the .ps1 file.
2. From the Start Screen, search for "PowerShell"
3. Right click PowerShell click "Run as Administrator"
4. Enter the command "Set-ExecutionPolicy RemoteSigned"
5. Enter "Y" at the prompt
6. Change directory to the folder containing the script (probably "cd Downloads")
7. Enter ".\InstallBrushes.ps1"
8. Now enter "Add-Brushes -InstallFrom D:\Brushes\" replacing the 'D:\Brushes\' location with your fonts folder

### TODO ###

* Make this a module
* Sign it
* Make instructions to install module
* Add more comments

### Feedback ###

Easiest way to submit feedback for this project is probably just to message me through BitBucket, or you could submit an issue. This is just a small script, so there won't be any real elaborate forms of communication for this project.